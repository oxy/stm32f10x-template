

#ifndef __USART_H
#define __USART_H

#include "stm32f10x.h"
#include "stdarg.h"
#include "stdio.h"

#define USART_PP USART1
#define USART_RCC (RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA)
#define USART_PORT GPIOA
#define USART_TX  GPIO_Pin_9
#define USART_RX  GPIO_Pin_10
#define USART_BAUD_RATE 115200   

void USART_Config(void);

void USART1_printf(USART_TypeDef *USARTx, uint8_t *Data, ...);

#endif


