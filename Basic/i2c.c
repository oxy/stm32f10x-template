#include "i2c.h"
#include "stdio.h"


static void I2C_NVIC_Init(void)
{
    NVIC_InitTypeDef NVIC_InitStruct;

    NVIC_InitStruct.NVIC_IRQChannel = I2C_EV_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 2;
    NVIC_Init(&NVIC_InitStruct);

    NVIC_InitStruct.NVIC_IRQChannel = I2C_ER_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 3;
    NVIC_Init(&NVIC_InitStruct);

    I2C_ITConfig(I2C_PP, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR, ENABLE);

}


static void I2C_GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    I2C_RCC_CLOCK_ENABLE;

    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_OD;
    GPIO_InitStruct.GPIO_Pin = I2C_SDA | I2C_SCL;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;

    GPIO_Init(I2C_PORT, &GPIO_InitStruct);

}


void I2C_Config(void)
{

    I2C_InitTypeDef I2C_InitStruct;

    I2C_DeInit(I2C_PP);

    I2C_NVIC_Init();

    I2C_GPIO_Init();

    I2C_InitStruct.I2C_Mode = I2C_Mode_I2C;
    I2C_InitStruct.I2C_DutyCycle = I2C_DutyCycle_2;
    I2C_InitStruct.I2C_OwnAddress1 = I2C_OWN_ADDRESS7  ;
    I2C_InitStruct.I2C_Ack = I2C_Ack_Enable;
    I2C_InitStruct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
    I2C_InitStruct.I2C_ClockSpeed = I2C_SPEED;

    I2C_Init(I2C_PP, &I2C_InitStruct);
    I2C_Cmd(I2C_PP, ENABLE);

}




void I2C1_EV_IRQHandler(void)
{

    switch(I2C_GetLastEvent(I2C_PP))
    {
        //slave
        case I2C_EVENT_SLAVE_BYTE_TRANSMITTED:
            break;

        case I2C_EVENT_SLAVE_BYTE_TRANSMITTING:
            break;

        case I2C_EVENT_SLAVE_BYTE_RECEIVED:
            break;

        case I2C_EVENT_SLAVE_RECEIVER_ADDRESS_MATCHED:
            break;

        case I2C_EVENT_SLAVE_TRANSMITTER_ADDRESS_MATCHED:
            break;

        case I2C_EVENT_SLAVE_RECEIVER_SECONDADDRESS_MATCHED:
            break;

        case I2C_EVENT_SLAVE_TRANSMITTER_SECONDADDRESS_MATCHED:
            break;

        case I2C_EVENT_SLAVE_GENERALCALLADDRESS_MATCHED:
            break;

        case I2C_EVENT_SLAVE_ACK_FAILURE:
            break;

        case I2C_EVENT_SLAVE_STOP_DETECTED:
            break;


        //master
        case I2C_EVENT_MASTER_MODE_SELECT:

            printf("I2C_EVENT_MASTER_MODE_SELECT\n");

            break;

        case I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED:

            printf("I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED\n");

            break;

        case I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED:

            printf("I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED\n");

            break;

        case I2C_EVENT_MASTER_BYTE_RECEIVED:

            printf("I2C_EVENT_MASTER_BYTE_RECEIVED\n");

            break;

        case I2C_EVENT_MASTER_BYTE_TRANSMITTED:

            printf("I2C_EVENT_MASTER_BYTE_TRANSMITTED\n");

            break;

        case I2C_EVENT_MASTER_BYTE_TRANSMITTING:

            printf("I2C_EVENT_MASTER_BYTE_TRANSMITTING\n");

            break;

        case I2C_EVENT_MASTER_MODE_ADDRESS10:

            printf("I2C_EVENT_MASTER_MODE_ADDRESS10\n");

            break;

    }


}


void I2C1_ER_IRQHandler(void)
{

    if(I2C_GetITStatus(I2C_PP, I2C_IT_AF))
    {

        I2C_ClearITPendingBit(I2C_PP, I2C_IT_AF);

    }


    if(I2C_GetITStatus(I2C_PP, I2C_IT_BERR))
    {

        I2C_ClearITPendingBit(I2C_PP, I2C_IT_BERR);

    }

}




