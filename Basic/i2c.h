
#ifndef __I2C_H
#define __I2C_H

#include "stm32f10x.h"

#define I2C_PP I2C1
#define I2C_OWN_ADDRESS7 0x0A
#define I2C_SPEED 400000
#define I2C_PORT GPIOB
#define I2C_SDA GPIO_Pin_6
#define I2C_SCL GPIO_Pin_7

#define I2C_EV_IRQn I2C1_EV_IRQn
#define I2C_ER_IRQn I2C1_ER_IRQn

#define I2C_RCC_CLOCK_ENABLE \
    do{      \
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);    \
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);     \
    } while(0)


void I2C_Config(void);

#endif
