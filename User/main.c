#include "stm32f10x.h"
#include "led.h"
#include "delay.h"
#include "usart.h"
#include "i2c.h"

static void NVIC_Config(void);


int main (void)
{
    NVIC_Config();

    USART_Config();

    I2C_Config();

    LED_Init();

    while(1)
    {

        printf("------>(%s-%s): NO\n", __DATE__, __TIME__);

        LED(ON);
        delay_s(1);

        printf("------>(%s-%s): OFF\n", __DATE__, __TIME__);
        LED(OFF);
        delay_s(1);
    }
}



static void NVIC_Config(void)
{
    
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

}

