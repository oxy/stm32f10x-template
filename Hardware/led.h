#ifndef __LED_H
#define __LED_H

#include "stm32f10x.h"

#define ON 0
#define OFF 1

#define LED(c) \
    do{  \
        if(c) GPIO_SetBits(GPIOC, GPIO_Pin_13);  \
        else GPIO_ResetBits(GPIOC, GPIO_Pin_13);  \
    }while(0)

void LED_Init(void);

#endif
